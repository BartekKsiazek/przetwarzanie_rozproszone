﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLight : MonoBehaviour {

	MeshRenderer red, yellow, green;

	void Start () {
		green = transform.GetChild (0).gameObject.GetComponent<MeshRenderer>();
		red = transform.GetChild (1).gameObject.GetComponent<MeshRenderer>();
		yellow = transform.GetChild (2).gameObject.GetComponent<MeshRenderer>();
	}

	void Update () {
		
	}

	public void SetColor(Lights lights) {
		
		if (lights == Lights.red) {
			//red set on
			red.sharedMaterial = CrossroadsController._this.redMat;
			//yellow set off
			yellow.sharedMaterial = CrossroadsController._this.blackMat;
			//green set off
			green.sharedMaterial = CrossroadsController._this.blackMat;
		
		} else if (lights == Lights.yellow) {
			//red set off
			red.sharedMaterial = CrossroadsController._this.blackMat;
			//yellow set on
			yellow.sharedMaterial = CrossroadsController._this.yellowMat;
			//green set off
			green.sharedMaterial = CrossroadsController._this.blackMat;
		
		} else {
			//red set off
			red.sharedMaterial = CrossroadsController._this.blackMat;
			//yellow set off
			yellow.sharedMaterial = CrossroadsController._this.blackMat;
			//green set on
			green.sharedMaterial = CrossroadsController._this.greenMat;
			
		}
	}

	public enum Lights{
		red,
		yellow,
		green
	};
}
