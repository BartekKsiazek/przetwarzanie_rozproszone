﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossroadsController : MonoBehaviour {

	public TrafficLight[] trafficLights;
	public GameObject[] triggers;
	public Material redMat, yellowMat, greenMat, blackMat;
	public bool started = false;
	public int setting = 0;
	public float time = 2f;

	public static CrossroadsController _this;

	//0 - R1
	//1 - R2
	//2 - L1
	//3 - L2
	//4 - D
	//5 - U

	void Start () {
		_this = this;
	}

	void Update () {
		
		if (Input.GetKeyUp (KeyCode.O))
			StartSetting1 ();
		else if (Input.GetKeyUp (KeyCode.P))
			StartSetting2 ();

		if (Input.GetKeyUp (KeyCode.S))
			StartCoroutine (SwitchLights ());

	}

	//main road
	void StartSetting1(){
		trafficLights [0].SetColor (TrafficLight.Lights.green);
		trafficLights [1].SetColor (TrafficLight.Lights.green);
		trafficLights [2].SetColor (TrafficLight.Lights.green);
		trafficLights [3].SetColor (TrafficLight.Lights.green);
		trafficLights [4].SetColor (TrafficLight.Lights.red);
		trafficLights [5].SetColor (TrafficLight.Lights.red);
	}

	void StartSetting2(){
		trafficLights [0].SetColor (TrafficLight.Lights.red);
		trafficLights [1].SetColor (TrafficLight.Lights.red);
		trafficLights [2].SetColor (TrafficLight.Lights.red);
		trafficLights [3].SetColor (TrafficLight.Lights.red);
		trafficLights [4].SetColor (TrafficLight.Lights.green);
		trafficLights [5].SetColor (TrafficLight.Lights.green);
	}


	IEnumerator SwitchLights(){

		if (setting == 0) {
			StartSetting2 ();
			setting = 1;
		} else if (setting == 1) {
			StartSetting1 ();
			setting = 0;
		}

		yield return new WaitForSeconds (time);
		StartCoroutine (SwitchLights ());

	}
}
